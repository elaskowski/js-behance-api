import axios from 'axios';

export const fetchUserData = username => async (dispatch) => {
  try {
    const { data } = await axios.get(`/userData/${username}`);
    dispatch(setUserData(data));
  } catch (err) {
    console.error(err);
    dispatch(setUserData('Not Found'));
  }
};

export const fetchUserProjects = username => async (dispatch) => {
  try {
    const projectArr = await axios.get(`/userProjects/${username}`);
    dispatch(setUserProjects(projectArr));
  } catch (err) {
    console.error(err);
  }
};

export const fetchUserWork = username => async (dispatch) => {
  try {
    const { data } = await axios.get(`/userWork/${username}`);
    dispatch(setUserWork(data));
  } catch (err) {
    console.error(err);
  }
};

export const fetchUserFollowers = username => async (dispatch) => {
  try {
    const { data } = await axios.get(`/userFollowers/${username}`);
    dispatch(setUserFollowers(data));
  } catch (err) {
    console.error(err);
  }
};

export const fetchUserFollowing = username => async (dispatch) => {
  try {
    const { data } = await axios.get(`/userFollowing/${username}`);
    dispatch(setUserFollowing(data));
  } catch (err) {
    console.error(err);
  }
};

function setUserData(data) {
  return {
    type: 'SET_USER_DATA',
    data,
  };
}

function setUserProjects(data) {
  return {
    type: 'SET_USER_PROJECTS',
    data,
  };
}

function setUserWork(data) {
  return {
    type: 'SET_USER_WORK',
    data,
  };
}

function setUserFollowers(data) {
  return {
    type: 'SET_USER_FOLLOWERS',
    data,
  };
}

function setUserFollowing(data) {
  return {
    type: 'SET_USER_FOLLOWING',
    data,
  };
}
