import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import LandingPage from '../components/LandingPage/LandingPage';

Enzyme.configure({ adapter: new Adapter() });

describe('Follower List Component', () => {
  let landingPage;
  beforeAll(() => {
    window.localStorage.setItem('state', JSON.stringify({ username: 'test' }));
    landingPage = shallow(
      <LandingPage />,
    );
  });
  it('should render correctly', () => {
    expect(landingPage).toMatchSnapshot();
  });
});
