import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Provider } from 'react-redux';
import FollowerList from '../components/Follows/FollowerList';

Enzyme.configure({ adapter: new Adapter() });

describe('Follower List Component', () => {
  let followerList;
  beforeAll(() => {
    window.localStorage.setItem('state', JSON.stringify({ username: 'test' }));
    const store = {
      fetchUserFollowers: jest.fn(),
      subscribe: jest.fn(),
      dispatch: jest.fn(),
      getState: jest.fn(),
    };
    followerList = shallow(
      <Provider store={store}>
        <FollowerList />
      </Provider>,
    );
  });
  it('should render correctly', () => {
    expect(followerList).toMatchSnapshot();
  });
});
