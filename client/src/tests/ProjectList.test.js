import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Provider } from 'react-redux';
import ProjectList from '../components/Projects/ProjectList';

Enzyme.configure({ adapter: new Adapter() });

describe('Project List Component', () => {
  let projectList;
  beforeAll(() => {
    window.localStorage.setItem('state', JSON.stringify({ username: 'test' }));
    const store = {
      fetchUserProjects: jest.fn(),
      subscribe: jest.fn(),
      dispatch: jest.fn(),
      getState: jest.fn(),
    };
    projectList = shallow(
      <Provider store={store}>
        <ProjectList />
      </Provider>,
    );
  });
  it('should render correctly', () => {
    expect(projectList).toMatchSnapshot();
  });
});
