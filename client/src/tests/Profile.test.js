import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Provider } from 'react-redux';
import Profile from '../components/Profile/Profile';

Enzyme.configure({ adapter: new Adapter() });

describe('Profile Component', () => {
  let profile;
  beforeAll(() => {
    const store = {
      fetchUserData: jest.fn(),
      subscribe: jest.fn(),
      dispatch: jest.fn(),
      getState: jest.fn(),
    };
    profile = shallow(
      <Provider store={store}>
        <Profile />
      </Provider>,
    );
  });
  it('should render correctly', () => {
    expect(profile).toMatchSnapshot();
  });
});
