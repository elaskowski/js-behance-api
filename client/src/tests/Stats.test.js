import React from 'react';
import Enzyme, { shallow, mount, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Provider } from 'react-redux';
import Stats from '../components/Stats/Stats';

Enzyme.configure({ adapter: new Adapter() });

describe('Stats Component', () => {
  let stats;
  beforeAll(() => {
    window.localStorage.setItem('state', JSON.stringify({ username: 'test' }));
    const store = {
      subscribe: jest.fn(),
      dispatch: jest.fn(),
      getState: jest.fn(),
    };
    stats = shallow(
      <Provider store={store}>
        <Stats />
      </Provider>,
    );
  });
  it('should render correctly', () => {
    expect(stats).toMatchSnapshot();
  });
});
