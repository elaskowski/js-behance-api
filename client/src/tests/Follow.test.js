import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Follow from '../components/Follows/Follow';

Enzyme.configure({ adapter: new Adapter() });

describe('Follow Component', () => {
  let follow;
  beforeAll(() => {
    const sample = {
      url: '123',
      image: 'test',
      username: 'test',
    };
    follow = shallow(
      <Follow follow={sample} />,
    );
  });
  it('should render correctly', () => {
    expect(follow).toMatchSnapshot();
  });
});
