import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Provider } from 'react-redux';
import FollowingList from '../components/Follows/FollowerList';

Enzyme.configure({ adapter: new Adapter() });

describe('Following List Component', () => {
  let followingList;
  beforeAll(() => {
    window.localStorage.setItem('state', JSON.stringify({ username: 'test' }));
    const store = {
      fetchUserFollowing: jest.fn(),
      subscribe: jest.fn(),
      dispatch: jest.fn(),
      getState: jest.fn(),
    };
    followingList = shallow(
      <Provider store={store}>
        <FollowingList />
      </Provider>,
    );
  });
  it('should render correctly', () => {
    expect(followingList).toMatchSnapshot();
  });
});
