import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Work from '../components/Work/Work';

Enzyme.configure({ adapter: new Adapter() });

describe('Work Component', () => {
  let work;
  beforeAll(() => {
    const sample = {
      position: 'test',
      organization: 'test',
      start_date: 'test',
      end_date: 'test',
      location: 'test',
    };
    work = shallow(
      <Work work={sample} />,
    );
  });
  it('should render correctly', () => {
    expect(work).toMatchSnapshot();
  });
});
