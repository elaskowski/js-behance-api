import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Provider } from 'react-redux';
import WorkList from '../components/Work/WorkList';

Enzyme.configure({ adapter: new Adapter() });

describe('Work List Component', () => {
  let workList;
  beforeAll(() => {
    window.localStorage.setItem('state', JSON.stringify({ username: 'test' }));
    const store = {
      fetchUserWork: jest.fn(),
      subscribe: jest.fn(),
      dispatch: jest.fn(),
      getState: jest.fn(),
    };
    workList = shallow(
      <Provider store={store}>
        <WorkList />
      </Provider>,
    );
  });
  it('should render correctly', () => {
    expect(workList).toMatchSnapshot();
  });
});
