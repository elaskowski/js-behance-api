import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Project from '../components/Projects/Project';

Enzyme.configure({ adapter: new Adapter() });

describe('Project Component', () => {
  let project;
  beforeAll(() => {
    const sample = {
      url: '123',
      name: 'test',
    };
    project = shallow(
      <Project project={sample} />,
    );
  });
  it('should render correctly', () => {
    expect(project).toMatchSnapshot();
  });
});
