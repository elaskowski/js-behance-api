export const userReducer = (state = null, action) => {
  switch (action.type) {
    case 'SET_USER_DATA':
      return action.data;
    default:
      return state;
  }
};

export const projectReducer = (state = null, action) => {
  switch (action.type) {
    case 'SET_USER_PROJECTS':
      return action.data;
    default:
      return state;
  }
};

export const workReducer = (state = null, action) => {
  switch (action.type) {
    case 'SET_USER_WORK':
      return action.data;
    default:
      return state;
  }
};

export const followerReducer = (state = null, action) => {
  switch (action.type) {
    case 'SET_USER_FOLLOWERS':
      return action.data;
    default:
      return state;
  }
};

export const followingReducer = (state = null, action) => {
  switch (action.type) {
    case 'SET_USER_FOLLOWING':
      return action.data;
    default:
      return state;
  }
};
