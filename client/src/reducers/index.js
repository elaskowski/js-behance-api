import { combineReducers } from 'redux';
import {
  userReducer, projectReducer, workReducer, followerReducer, followingReducer, usernameReducer,
} from './userReducers';

const rootReducer = combineReducers({
  user: userReducer,
  projects: projectReducer,
  work: workReducer,
  followers: followerReducer,
  following: followingReducer,
});

export default rootReducer;
