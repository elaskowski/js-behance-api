import React, { Component } from 'react';

import './LandingPage.css';

class LandingPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
    };
  }

  handleInput(e) {
    this.setState({ username: e.target.value });
  }

  handleSubmit(e) {
    e.preventDefault();
    try {
      const serializedState = JSON.stringify({ username: this.state.username });
      localStorage.setItem('state', serializedState);
      this.props.history.push('/profile');
    } catch (err) {
      console.error(err);
    }
  }

  render() {
    return (
      <div className="search-form">
        <div className="landing-page-title">
            Behance Search
        </div>
        <form onSubmit={e => this.handleSubmit(e)}>
          {' '}
          <input onChange={e => this.handleInput(e)} placeholder="Enter a username" required />
          {' '}
          <br />
          <br />
          <button>
            Search
          </button>
        </form>
      </div>
    );
  }
}

export default LandingPage;
