import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchUserData } from '../../actions';
import Stats from '../Stats/Stats';
import ProjectList from '../Projects/ProjectList';
import WorkList from '../Work/WorkList';
import FollowerList from '../Follows/FollowerList';
import FollowingList from '../Follows/FollowingList';

import './Profile.css';

class Profile extends Component {
  componentDidMount() {
    const { username } = JSON.parse(localStorage.getItem('state'));
    this.props.fetchUserData(username);
  }

  render() {
    if (this.props.user === 'Not Found') {
      return (
        <div className="user-error-message">
          User does not exist!
        </div>
      );
    }
    return (
      <div className="row">
        <div className="col-md-3">
          <Stats />
          <FollowingList />
          <FollowerList />
        </div>
        <div className="col-md-5">
          <ProjectList />
        </div>
        <div className="col-md-4">
          <WorkList />
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    fetchUserData,
  },
  dispatch,
);

export default connect(state => ({
  user: state.user,
}), mapDispatchToProps)(Profile);
