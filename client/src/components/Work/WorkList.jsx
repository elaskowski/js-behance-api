import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchUserWork } from '../../actions';
import Work from './Work';

import './Work.css';

class WorkList extends Component {
  componentDidMount() {
    const { username } = JSON.parse(localStorage.getItem('state'));
    this.props.fetchUserWork(username);
  }

  render() {
    return (
      <div>
        <div className="work-container">
          <h2>
          WORK EXPERIENCE
          </h2>
          {this.props.work && (
          <div>
            {this.props.work.map(work => (
              <Work work={work} key={work.position + new Date().getTime()} />
            ))}
          </div>
          )}
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    fetchUserWork,
  },
  dispatch,
);

export default connect(state => ({
  work: state.work,
}), mapDispatchToProps)(WorkList);
