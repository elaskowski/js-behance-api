import React from 'react';

import './Work.css';

const Work = props => (
  <div className="work">
    <h5>
      {props.work.position}
    </h5>
    <div>
      {props.work.organization}
    </div>
    <div>
      {props.work.start_date}
      {' '}
      to
      {' '}
      {props.work.end_date}
    </div>
    <div>
      {props.work.location}
    </div>
  </div>
);

export default Work;
