import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import LandingPage from '../LandingPage/LandingPage';
import Profile from '../Profile/Profile';

import './App.css';

class App extends Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div>
        <BrowserRouter>
          <Switch>
            <Route path="/profile" component={Profile} />
            <Route exact path="/" component={LandingPage} />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
