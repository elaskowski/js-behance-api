import React from 'react';

const Project = props => (
  <div>
    <a className="project-url" href={props.project.url}>
      {props.project.name}
    </a>
  </div>
);

export default Project;
