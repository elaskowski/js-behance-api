import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchUserProjects } from '../../actions';
import Project from './Project';

import './ProjectList.css';

class ProjectList extends Component {
  componentDidMount() {
    const { username } = JSON.parse(localStorage.getItem('state'));
    this.props.fetchUserProjects(username);
  }

  render() {
    return (
      <div>
        <div className="projects-container">
          <h2>
            PROJECTS
          </h2>
          {(!this.props.projects) && (
          <div className="loading" />
          )}
          {this.props.projects && (
            <div>
              {this.props.projects.data.map(project => (
                <Project project={project} key={project.id} />
              ))}
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    fetchUserProjects,
  },
  dispatch,
);

export default connect(state => ({
  projects: state.projects,
}), mapDispatchToProps)(ProjectList);
