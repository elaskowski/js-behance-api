import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchUserFollowing } from '../../actions';
import Follow from './Follow';

import './Follows.css';

class FollowingList extends Component {
  componentDidMount() {
    const { username } = JSON.parse(localStorage.getItem('state'));
    this.props.fetchUserFollowing(username);
  }

  render() {
    return (
      <div>
        {this.props.following && this.props.user && (
        <div className="follow-container">
          <div>
            <p id="section-title">
              {this.props.user.following}
              {' '}
              Following
            </p>
            {this.props.following.map(followee => (
              <Follow follow={followee} key={followee.id} />
            ))}
          </div>
        </div>
        )}
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    fetchUserFollowing,
  },
  dispatch,
);

export default connect(state => ({
  following: state.following,
  user: state.user,
}), mapDispatchToProps)(FollowingList);
