import React from 'react';

const Follow = props => (
  <div>
    <a href={props.follow.url}>
      <img src={props.follow.image} alt="User Image" />
    </a>
    <br />
    <a className="follow-url" href={props.follow.url}>
      {props.follow.username}
    </a>
  </div>
);

export default Follow;
