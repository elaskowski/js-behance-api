import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchUserFollowers } from '../../actions';
import Follow from './Follow';

import './Follows.css';

class FollowerList extends Component {
  componentDidMount() {
    const { username } = JSON.parse(localStorage.getItem('state'));
    this.props.fetchUserFollowers(username);
  }

  render() {
    return (
      <div>
        {this.props.followers && this.props.user && (
        <div className="follow-container">
          <div>
            <p id="section-title">
              {this.props.user.followers}
              {' '}
              Followers
            </p>
            {this.props.followers.map(follower => (
              <Follow follow={follower} key={follower.id} />
            ))}
          </div>
        </div>
        )}
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    fetchUserFollowers,
  },
  dispatch,
);

export default connect(state => ({
  followers: state.followers,
  user: state.user,
}), mapDispatchToProps)(FollowerList);
