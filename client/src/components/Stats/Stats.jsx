import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchUserData } from '../../actions';

import './Stats.css';

class Stats extends Component {
  render() {
    return (
      <div>
        {this.props.user && (
        <div className="user-container">
          <div>
            <img src={this.props.user.image} alt="User Profile Image" />
          </div>
          <p id="section-title">
            {this.props.user.username}
          </p>
          <p>
            {this.props.user.city}
          </p>
          <p>
            Appreciations:
            <br />
            {this.props.user.appreciations}
          </p>
          <p>
            Views:
            <br />
            {this.props.user.views}
          </p>
          <p>
            Comments:
            <br />
            {this.props.user.comments}
          </p>
        </div>
        )}
      </div>
    );
  }
}

export default connect(state => ({
  user: state.user,
}), null)(Stats);
