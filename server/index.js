const env = require('dotenv');
const path = require('path');
const express = require('express');
const parser = require('body-parser');
const helmet = require('helmet');
const axios = require('axios');

env.config({
  path: path.resolve(__dirname, '../.env'),
});

const port = process.env.PORT || 3000;
const key = process.env.API_KEY;

const app = express();

app.use(helmet());
app.use(parser.json());
app.use(parser.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, '../client/dist')));

app.get('/userData/:username', async (req, res) => {
  const { username } = req.params;
  try {
    const resource = await axios.get(`https://api.behance.net/v2/users/${username}?client_id=${key}`);
    const { user } = resource.data;
    const data = {
      username: user.username,
      firstName: user.first_name,
      lastName: user.last_name,
      city: user.city,
      image: user.images['138'],
      followers: user.stats.followers,
      following: user.stats.following,
      appreciations: user.stats.appreciations,
      views: user.stats.views,
      comments: user.stats.comments,
    };
    res.status(200).send(data);
  } catch (err) {
    res.status(404).send(`Error retrieving user data ${err}`);
  }
});

app.get('/userProjects/:username', async (req, res) => {
  const { username } = req.params;
  try {
    const resource = await axios.get(`https://api.behance.net/v2/users/${username}/projects?client_id=${key}`);
    const { projects } = resource.data;
    const projectArr = [];
    projects.forEach((project) => {
      const data = {
        id: project.id,
        name: project.name,
        url: project.url,
      };
      projectArr.push(data);
    });
    res.status(200).send(projectArr);
  } catch (err) {
    res.status(404).send(`Error retrieving user projects ${err}`);
  }
});

app.get('/userFollowers/:username', async (req, res) => {
  const { username } = req.params;
  try {
    const resource = await axios.get(`https://api.behance.net/v2/users/${username}/followers?client_id=${key}&per_page=5`);
    const { followers } = resource.data;
    const followerArr = [];
    followers.forEach((follower) => {
      const data = {
        id: follower.id,
        username: follower.username,
        url: follower.url,
        image: follower.images['50'],
      };
      followerArr.push(data);
    });
    res.status(200).send(followerArr);
  } catch (err) {
    res.status(404).send(`Error retrieving user followers ${err}`);
  }
});

app.get('/userFollowing/:username', async (req, res) => {
  const { username } = req.params;
  try {
    const resource = await axios.get(`https://api.behance.net/v2/users/${username}/following?client_id=${key}&per_page=5`);
    const { following } = resource.data;
    const followingArr = [];
    following.forEach((followee) => {
      const data = {
        id: followee.id,
        username: followee.username,
        url: followee.url,
        image: followee.images['50'],
      };
      followingArr.push(data);
    });
    res.status(200).send(followingArr);
  } catch (err) {
    res.status(404).send(`Error retrieving user following ${err}`);
  }
});

app.get('/userWork/:username', async (req, res) => {
  const { username } = req.params;
  try {
    const resource = await axios.get(`https://api.behance.net/v2/users/${username}/work_experience?client_id=${key}`);
    const { work_experience } = resource.data;
    res.status(200).send(work_experience);
  } catch (err) {
    res.status(404).send(`Error retrieving user work ${err}`);
  }
});

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../client/dist/index.html'));
});

app.listen(port, (err) => {
  if (err) throw new Error();
  console.log('successfully connected to port', port);
});
